public class Sala {

private int capacidad;
private int asientosReservados;

public Sala(){
	
	capacidad=60;
	asientosReservados=0;
	
	}//fin metodo constructor sin parametros
	
public Sala (int capacidad, int asientosReservados){
	
	this.capacidad=capacidad;
	this.asientosReservados=asientosReservados;
	
	}//fin metodo constructor con parametros
	
public void setCapacidad(int capacidad){
	
	this.capacidad=capacidad;
	
	}//fin metodo setCapacidad
	
public int getCapacidad(){
	
	return capacidad;
	}//fin metodo getCapacidad
	
public void setAsientosReservados(int asientosReservados){
	
	this.asientosReservados=asientosReservados;
	
	}//fin metodo setCapacidad
	
public int getAsientosReservados(){
	
	return asientosReservados;
	}//fin metodo getCapacidad

} //fin clase
